import React from "react";
import { Switch, Route } from "react-router-dom";
import LoginPage from "../pages/LoginPage";
import { useSelector } from "react-redux";
import { MODULE_NAME as MODULE_AUTHOR } from "../modules/Author/constants/models";
import MainLayout from "./HOCS/MainLayout";
import MembersPage from "../pages/MembersPage";
import Sangpage from "../pages/Sangpage";
import Kiemdinhvien from "../pages/Kiemdinhvien";
import MinhChungPage from "../pages/MinhChungPage";
import MinhChungDetailPage from "../pages/MinhChungDetailPage";
import AnimatedSwitch from "./components/Routes/AnimatedSwitch";

const Routes = () => {
  const isSigned = useSelector((state) => state[MODULE_AUTHOR].isSigned);
  // sang sửa thêm && info==1
  console.log("isSigned", isSigned);
  const {info} = useSelector((state) => state.user)
  if(info)
  //console.log("QUyền hiện tại:", info.id);
  if (isSigned && info.id==1) {
        return (
        <MainLayout>
          <AnimatedSwitch>
            <Route exact path="/" component={() => <div>Analytics 1</div>} />
            <Route exact path="/members" component={MembersPage} />
            <Route exact path="/Sangpage" component={Sangpage} />
            <Route exact path="/minhchung" component={MinhChungPage} />            
            <Route exact path="/minhchungDetail/:idparam" component={MinhChungDetailPage} />
            <Route exact path="/minhchungDetail" component={MinhChungDetailPage} />
          </AnimatedSwitch>
        </MainLayout>
      );
  }

  else if (isSigned && info.id==2) {
    return (
    <MainLayout>
      <AnimatedSwitch>
        <Route exact path="/" component={() => <div>Analytics 1</div>} />
        <Route exact path="/members" component={MembersPage} />
        <Route exact path="/Sangpage" component={Sangpage} />
        <Route exact path="/minhchung" component={MinhChungPage} />       
        <Route exact path="/minhchungDetail/:idparam" component={MinhChungDetailPage} />
        <Route exact path="/minhchungDetail" component={MinhChungDetailPage} />
      </AnimatedSwitch>
    </MainLayout>
    );
  }
  else if (isSigned && info.id==3) {
    return (
    <MainLayout>
      <AnimatedSwitch>
        <Route exact path="/" component={() => <div>Analytics 1</div>} />
        <Route exact path="/members" component={MembersPage} />
        <Route exact path="/Sangpage" component={Sangpage} />
        <Route exact path="/minhchung" component={MinhChungPage} />       
        <Route exact path="/minhchungDetail/:idparam" component={MinhChungDetailPage} />
        <Route exact path="/minhchungDetail" component={MinhChungDetailPage} />
      </AnimatedSwitch>
    </MainLayout>
    );
  }
  else if (isSigned && info.id==4){
  return (
    <MainLayout>
      <AnimatedSwitch>
        <Route exact path="/" component={() => <div>Analytics 1</div>} />
        <Route exact path="/members" component={MembersPage} />
        <Route exact path="/Sangpage" component={Sangpage} />
        <Route exact path="/minhchung" component={MinhChungPage} />       
        <Route exact path="/minhchungDetail/:idparam" component={MinhChungDetailPage} />
        <Route exact path="/minhchungDetail" component={MinhChungDetailPage} />
      </AnimatedSwitch>
    </MainLayout>
    );
  } 
  //sang kêt thúc
  
  return (
    <Switch>
      <Route path="/" component={LoginPage} />
    </Switch>
  );
  
};

export default Routes;
