import React from "react";
import MinhChung from "../modules/MinhChung/MinhChung";
import CustomBreadcrumbs from "../common/components/Breadcrumb";

const MinhChungPage = () => {
  return (
    <div className="page">
      <CustomBreadcrumbs title="Quản lý minh chứng" />   
    
      <MinhChung />
    </div>
  );
};

export default MinhChungPage;
