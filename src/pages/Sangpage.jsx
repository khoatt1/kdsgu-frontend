import React from "react";
import Sang from "../modules/Kiemdinhvien/Sang";
import CustomBreadcrumbs from "../common/components/Breadcrumb";

const Sangpage = () => {
  return (
    <div className="page">
      <CustomBreadcrumbs title="Thanh Sang" />
      <Sang />
    </div>
  );
};

export default Sangpage;
