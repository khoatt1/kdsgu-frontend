import React from "react";
import MinhChungDetail from "../modules/MinhChung/MinhChungDetail";
import CustomBreadcrumbs from "../common/components/Breadcrumb";

const MinhChungDetailPage = () => {
  return (
    <div className="page">
      <CustomBreadcrumbs title="Chi tiết minh chứng" />   
      <MinhChungDetail/>
    
    </div>
  );
};

export default MinhChungDetailPage;
