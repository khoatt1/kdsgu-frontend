import { clearAll } from "../../../common/redux/actions/uiActions";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

const initialState = { 
  minhChungList:[],
  minhchungDetail:'',
  donViList:[
    { id: '1', name: 'Khoa công nghệ thông tin' },
    { id: '2', name: 'Phòng khảo thí' }  
  ]
};
const handler = {  
  [actions.searchMinhChungAPI]: (state, action) => ({ ...state,minhChungList: action.payload,minhchungDetail:{}}),
  [actions.minhChungAPIById]: (state, action) => ({...state, minhchungDetail: action.payload}),
  [actions.clearData]: (state, action) => ({ ...state,minhChungList:[]}),
  [actions.minhchungAdd]: (state, action) => ({ ...state,minhchungDetail: action.payload}),
  [actions.minhchungUpdate]: (state, action) => ({ ...state,minhchungDetail: action.payload}),
};

export default handleActions(handler, initialState);
