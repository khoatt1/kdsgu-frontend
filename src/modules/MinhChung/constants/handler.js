import {  
  searchMinhChungAPI,
  clearData,
  minhChungAPIById,
  minhchungAdd,
  minhchungUpdate
} from "./actions";
import { fetchLoading, loading, loadingProcess,fetchLoadingByMethod } from "../../../common/effects";

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const urlAPI_temp = 'https://retoolapi.dev/wzwMDi/minhchung'

export function searchMinhChungApiAsync(params){
   let arrMinhChung = [];
  if (params && params.idminhchung){
    console.log('CO params');     
    return fetchLoading({
          url: urlAPI_temp + '/' + params.idminhchung,
          method: 'GET'
      }).then(response => {         
          arrMinhChung.push(response.data);    
          return arrMinhChung
      })
  }else{
       console.log('KO co params');  
       return fetchLoading({
        url: urlAPI_temp,
        method: 'GET'
    }).then(response => {
         response.data.forEach((item) => {
            arrMinhChung.push(
                {
                  id: item.id,
                  url: item.url,
                  donvi: item.donvi,
                  idminhchung: item.idminhchung,
                  tenminhchung: item.tenminhchung,
                  mota: item.mota
                }
              );
          })
          return arrMinhChung
    })
  }
 
}

export function addMinhChungAPIAsync(params){
 
  const formData = {
    "idminhchung": params.idminhchung,
    "tenminhchung": params.tenminhchung,
    "url": params.url,
    "donvi": params.donViId,
    "mota": params.motaminhchung, 
  }

  const requestHeaders = {

  }
    
  return fetchLoadingByMethod({
      method: "post",
      url: urlAPI_temp,   
      body:formData,
      headers:requestHeaders      
  }).then(response => {
      return response.data
  })
}

export function updateMinhChungAPIAsync(params){
 
  const formData = {
    "idminhchung": params.idminhchung,
    "tenminhchung": params.tenminhchung,
    "url": params.url,
    "donvi": params.donViId,
    "mota": params.motaminhchung, 
  }
  const requestHeaders = {

  }
   
  return fetchLoadingByMethod({
      method: "put",
      url: urlAPI_temp + '/' + params.id,
      body:formData,
      headers:requestHeaders           
  }).then(response => {
      return response.data
  })
}

export function deleteMinhChungAPIAsync(params){
 
  const formData = {    
  }
  const requestHeaders = {
  }   
  return fetchLoadingByMethod({
      method: "delete",
      url: urlAPI_temp + '/' + params.id,
      body:formData,
      headers:requestHeaders           
  }).then(response => {
      return response.data
  })
}



export function getMinhChungApiAsyncById(idMinhChung){
  return fetchLoading({
      url: urlAPI_temp + '/' + idMinhChung,
      method: 'GET'
  }).then(response => {
      return response.data
  })
}

export default (dispatch, props) => ({
  searchMinhChungAPI: async (params) => {
    try {
      console.log('Call API MC')    
      console.log('params: ',params)   
      const minhChungList = await searchMinhChungApiAsync(params)     
      console.log('result', minhChungList);     
      dispatch(searchMinhChungAPI(minhChungList))       
    } catch (error){
        
    }
  },

  addOrUpdateMinhChungAPI: async (params) => {
    try {
      if (params.id === undefined || params.id === ''){
        console.log('THEM MOI')  
        console.log('params: ',params)   
        const minhchungDetail = await addMinhChungAPIAsync(params) 
        dispatch(minhchungAdd(minhchungDetail))       
      }else{
        console.log('CHINH SUA')  
        console.log('params: ',params)   
        const minhchungDetail = await updateMinhChungAPIAsync(params)
        dispatch(minhchungUpdate(minhchungDetail))      
      }   
     
    } catch (error){
        
    }
  },

  deleteMinhChungAPI: async (params) => {
    try {      
        //thuc hien xoa
        await deleteMinhChungAPIAsync(params) 
        //select lai danh sach
        const minhChungList = await searchMinhChungApiAsync()     
        dispatch(searchMinhChungAPI(minhChungList))     
    } catch (error){
       
    }
  },

  clearData: async (params) => {
    try {        
      dispatch(clearData()) //gui action vao reducer
    } catch (error){

    }
  },

  getMinhChungAPIById: async (params) => {
    try {
      if (params){      
        const minhchungDetail = await getMinhChungApiAsyncById(params)         
        dispatch(minhChungAPIById(minhchungDetail))  
        console.log('getMinhChungAPIById', 'OK');  
        return minhchungDetail;      
      }      
    } catch (error){

    }
  },
});
