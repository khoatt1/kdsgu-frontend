import { createAction } from "redux-actions";
import { MODULE_NAME } from "../../MinhChung/constants/models";

export const searchMinhChungAPI  = createAction(`@${MODULE_NAME}/searchMinhChungAPI`);
export const clearData  = createAction(`@${MODULE_NAME}/clearData`);
export const minhChungAPIById  = createAction(`@${MODULE_NAME}/minhChungAPIById`);
export const minhchungAdd  = createAction(`@${MODULE_NAME}/minhchungAdd`);
export const minhchungUpdate  = createAction(`@${MODULE_NAME}/minhchungUpdate`);