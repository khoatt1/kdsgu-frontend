import React, { useState, useMemo, useEffect } from "react";
import TextField from '@material-ui/core/TextField'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import { makeStyles } from '@material-ui/core/styles';
import handler from "./constants/handler";
import { useSelector, useDispatch,connect } from "react-redux";//connect để ket noi voi store
import { MODULE_NAME } from "./constants/models";
import { setSessionLoading } from "../../common/redux/actions/session";
import { useHistory,useParams } from "react-router-dom";

const useStyles = makeStyles({
  backgroundForm: { 
    padding: "5px"   
  }
});

export default function MinhChungDetail(props) {    
    const { idparam } = useParams();     
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();    
    const {addOrUpdateMinhChungAPI,getMinhChungAPIById} = useMemo(() => handler(dispatch), []);   
    const donViList =  useSelector(state => state.minhchung.donViList);
    const detail =  useSelector(state => state.minhchung.minhchungDetail);

    const donViOptions = donViList.map(donVi => (
      <option key={donVi.id} value={donVi.id}>
        {donVi.name}
      </option>
    ))
    
    const [id, setId] = useState(detail.id)    
    const [idminhchung, setIdminhchung] = useState('')
    const [tenminhchung, setTenminhchung] = useState('')
    const [donViId, setDonViId] = useState('')
    const [motaminhchung, setMotaminhchung] = useState('')

    useEffect(() => {  
        if (idparam){
            getMinhChungAPIById(idparam).then(detailMC => {
                setIdminhchung(detailMC.idminhchung);
                setTenminhchung(detailMC.tenminhchung);
                setDonViId(detailMC.donvi);
                setMotaminhchung(detailMC.mota);
            }); 
        }            
    },[]);     
      
    const onIdminhchungChanged = e => setIdminhchung(e.target.value)
    const onTenminhchungChanged = e => setTenminhchung(e.target.value) 
    const onDonViChanged = e => setDonViId(e.target.value)  
    const onMotaminhchungChanged = e => setMotaminhchung(e.target.value) 
    const handlerSubmitMinhChung = (event) => {       
      addOrUpdateMinhChungAPI({
        id:detail.id,
        idminhchung,
        tenminhchung,
        donViId,
        motaminhchung
      });
      console.log('minhchungDetail.id: ',detail.id);            
    }

  return (
    <div className="container-fluid">    
         <p> You were redirected from {idparam}</p>   
        <div className="row p-2">     
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">ID minh chứng</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <input className="form-control" 
                  type="text"
                  id="idminhchung"
                  name="idminhchung"
                  value={idminhchung}
                  onChange={onIdminhchungChanged}
                />
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">Tên minh chứng</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <input className="form-control" 
                  type="text"
                  id="tenminhchung"
                  name="tenminhchung"
                  value={tenminhchung}
                  onChange={onTenminhchungChanged}
                />
            </div>
        </div>  
        <div className="row p-2">     
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">Đơn vị</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <select id="donVi" 
                    value={donViId} 
                    onChange={onDonViChanged} 
                    className="form-select">
                      <option value=""></option>
                      {donViOptions}
                  </select>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">Mô tả</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <input className="form-control" 
                  type="text"
                  id="motaminhchung"
                  name="motaminhchung"
                  value={motaminhchung}
                  onChange={onMotaminhchungChanged}
                />
            </div>
        </div>            
       <div className="row">              
          <div className="col-xs- col-sm- col-md- col-lg- text-center p-2">             
            <button type="button" className="btn btn-primary" onClick = {handlerSubmitMinhChung}>Lưu</button>
            <span> </span>            
                <button type="button" className="btn btn-warning" 
                onClick={() => {history.push('/minhchung')}}>Quay lại</button>
          </div>          
       </div>
    </div>
  )
}