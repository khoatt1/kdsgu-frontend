import {
  fetchMemberPending,
  fetchMemberSuccess,
  testAPI,
  fetchMemberFailure,
} from "./actions";
import { fetchLoading, loading, loadingProcess } from "../../../common/effects";

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// Tantm: Function test api
export function getApiAsync(){
  return fetchLoading({
      url: `http://localhost:5000/`,
      method: 'GET'
  }).then(response => {
      return response.data
  })
}

export default (dispatch, props) => ({
  fetchMember: async (params) => {
    try {
      await loadingProcess(
        () => wait(4000),
        () => dispatch(fetchMemberSuccess())
      );
    } catch (error) {
      console.log("======== Bao Minh: error", error);
      dispatch(fetchMemberFailure(error.message));
    }
  },
  testCallAPI: async (params) => {
    try {
      console.log('test call api parmas: ',params)
      // Tantm: Test API
      console.log('tantm: fetchMember')
      const test_api = await getApiAsync()
      console.log('Result: ',test_api)
      // tantm
      // Xuwr ly data
      // tantm: Đẩy dữ liệu lên store
      dispatch(testAPI(test_api))

    } catch (error){

    }
  }
});
