import React, { useState, useMemo, useEffect } from "react";
import TextField from '@material-ui/core/TextField'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import handler from "./constants/handler";
import { useSelector, useDispatch,connect } from "react-redux";//connect để ket noi voi store
import { MODULE_NAME } from "./constants/models";
import { setSessionLoading } from "../../common/redux/actions/session";
import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import { makeStyles } from "@material-ui/styles";
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import Icon from '@material-ui/core/Icon';
import LinkIcon from '@material-ui/icons/Link';
import { useHistory } from "react-router-dom";
const useStyles = makeStyles({
 
   myEditIcon: {
    color: "#01579b"   
  },
   myDeleteIcon: {
    color: "red"   
  },
  myAddIcon: {
    color: "green"   
  },
});

export default function MinhChung(props) {
   
     
      const history = useHistory();
      const dispatch = useDispatch();
      //useMemo dùng tránh render lại các hàm bên trong khi không cần thiết
      const {clearData,callMinhChungAPI} = useMemo(() => handler(dispatch, props), [
          dispatch,
          props,
      ]);
     
     //useSelector dùng để get các state từ store (gọi all state)
     // const {userList} = useSelector(
     //   (state) => state[MODULE_NAME]
     // );

      //gọi state cụ thể
      const minhChungList =  useSelector(state => state.minhchung.minhChungList)
     
      const handleSearch = (event) => {
        event.preventDefault();
        callMinhChungAPI();       
      }

      //thử clear value state
      const handleClearAll = (event) => {
        event.preventDefault();
        clearData();       
      }
     
      const [state, setState] = React.useState({
    columns: [
      { 
        title: 'ID minh chứng', 
        field: 'idminhchung',         
        headerStyle: {
            backgroundColor: '#01579b',
            color: '#FFF'
        }
      },
      { 
        title: 'Tên minh chứng', 
        field: 'tenminhchung' ,
         headerStyle: {
            backgroundColor: '#01579b',
            color: '#FFF'
        }
      },
      { 
        title: 'Đơn vị upload', 
        field: 'donvi' ,
         headerStyle: {
            backgroundColor: '#01579b',
            color: '#FFF'
        },
        lookup: { 1: 'Khoa công nghệ thông tin', 2: 'Phòng Khảo thí' },
      },     
      { 
        title: 'Mô tả minh chứng', 
        field: 'mota' ,
         headerStyle: {
            backgroundColor: '#01579b',
            color: '#FFF'
        }
      },
      {
        field: 'url',
        title: 'File upload',
        render: rowData => <LinkIcon color="secondary"/>
      }
      

    ],
    data: [
      { 
            id: 'H.001.111',
            tenminhchung: 'Quyết định tuyển dụng nhân sự', 
            donvi: 1, 
            mota: 'Quyết định tuyển dụng nhân sự' ,
            file: 'File upload' 
     },
     { 
            id: 'H.001.112',
            tenminhchung: 'Quyết định tuyển dụng nhân sự', 
            donvi: 1, 
            mota: 'Quyết định tuyển dụng nhân sự' ,
            file: 'File upload' 
     },
     { 
            id: 'H.001.113',
            tenminhchung: 'Quyết định tuyển dụng nhân sự', 
            donvi: 2, 
            mota: 'Quyết định tuyển dụng nhân sự' ,
            file: 'File upload' 
     },
     { 
            id: 'H.001.114',
            tenminhchung: 'Quyết định tuyển dụng nhân sự', 
            donvi: 1, 
            mota: 'Quyết định tuyển dụng nhân sự' ,
            file: 'File upload' 
     },
      //{name: 'Zerya Betül', surname: 'Baran', birthYear: 2017, birthCity: 34}
    ],
  });
 const classes = useStyles();
  return (
    <div className="container-fluid">
        <div className="row p-2">     
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">ID minh chứng</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <input type="text" name="" id="input" className="form-control" ></input>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">Tên minh chứng</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <input type="text" name="" id="input" className="form-control" />
            </div>        

        </div>
        <div className="row p-2">     
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">Đơn vị</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <input type="text" name="" id="input" className="form-control" />
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">Mô tả</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <input type="text" name="" id="input" className="form-control" />
            </div>        

        </div>
         <div className="row">              
          <div className="col-xs- col-sm- col-md- col-lg- text-center p-2">             
            <button type="button" className="btn btn-primary"  onClick = {handleSearch}>Tìm kiếm</button>
            <span> </span>            
                <button type="button" className="btn btn-warning"  onClick = {handleClearAll}>Hủy</button>
          </div>          
           
         </div>
         <MaterialTable
            title=""
            columns={state.columns}
            data={minhChungList}
            options={{
              headerStyle: {
                backgroundColor: '#01579b',
                color: '#FFF'
              }
            }}
          
            actions={[
              {
                icon: 'edit',
                iconProps: {
                  
                },
                tooltip: 'Chỉnh sửa',
                onClick: (event, rowData) => {history.push(`/minhchungedit/${rowData.id}`)}
              },
              {
                icon: 'delete',
                iconProps: {
                 
                },
                tooltip: 'Xóa',
                onClick: (event, rowData) => {
                  if (window.confirm("Bạn muốn xóa dữ liệu này? ") == true){
                    alert("Đã xóa dữ liệu", rowData.id);
                  }
                }                  
              },
              {
                icon: 'add_circle',
                iconProps: {
                 
                },
                tooltip: 'Thêm mới',
                isFreeAction: true,
                onClick: (event) => alert("You want to add a new row")
              }
            ]}
          />
    </div>
    
  )
}