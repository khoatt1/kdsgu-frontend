import { createAction } from "redux-actions";
import { MODULE_NAME } from "../../Author/constants/models";

export const fetchSangPending = createAction(`@${MODULE_NAME}/FETCH_SANG_PENDING`);
export const setEmpty = createAction(`@${MODULE_NAME}/SET_EMPTY`);
export const setFilter = createAction(`@${MODULE_NAME}/SET_FILTER`);
export const resetFilter = createAction(`@${MODULE_NAME}/RESET_FILTER`);
export const fetchSangSuccess  = createAction(`@${MODULE_NAME}/FETCH_SANG_SUCCESS`);
export const fetchSangFailure  = createAction(`@${MODULE_NAME}/FETCH_SANG_FAILURE`);

// tantm
export const testAPI  = createAction(`@${MODULE_NAME}/testAPI`);