import {
  fetchMemberPending,
  fetchMemberSuccess,
  testAPI,
  fetchMemberFailure,
  minhChungAPI,
  clearData,
  minhChungAPIById
} from "./actions";
import { fetchLoading, loading, loadingProcess } from "../../../common/effects";

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// Tantm: Function test api
export function getApiAsync(){
  return fetchLoading({
      url: `https://gorest.co.in/public/v1/users`,
      method: 'GET'
  }).then(response => {
      return response.data
  })
}

export function getMinhChungApiAsync(){
  return fetchLoading({
      url: `https://retoolapi.dev/Rfe6gD/minhchung`,
      method: 'GET'
  }).then(response => {
      return response.data
  })
}

export function getMinhChungApiAsyncById(idMinhChung){
  return fetchLoading({
      url: 'https://retoolapi.dev/Rfe6gD/minhchung/' + idMinhChung,
      method: 'GET'
  }).then(response => {
      return response.data
  })
}

export default (dispatch, props) => ({
  fetchMember: async (params) => {
    try {
      await loadingProcess(
        () => wait(1000),
        () => dispatch(fetchMemberSuccess())
      );
    } catch (error) {
      console.log("======== Bao Minh: error", error);
      dispatch(fetchMemberFailure(error.message));
    }
  }, 
  callMinhChungAPI: async (params) => {
    try {
      console.log('Call API MC: ',params)    
      const minhChungList = await getMinhChungApiAsync()//thuc hien call api
      console.log('Result: ',minhChungList)      
      dispatch(minhChungAPI(minhChungList)) //gui action vao reducer
    } catch (error){

    }
  },

  clearData: async (params) => {
    try {        
      dispatch(clearData()) //gui action vao reducer
    } catch (error){

    }
  },

  callMinhChungAPIById: async (params) => {
    try {
      console.log('Call API minh chung chi tiet: ',params)    
      const minhchungDetail = await getMinhChungApiAsyncById(2)//thuc hien call api - tam load cứng id = 2
      console.log('Result: ',minhchungDetail)      
      dispatch(minhChungAPIById(minhchungDetail)) //gui action vao reducer
      console.log('huy: ',minhchungDetail)   
    } catch (error){

    }
  },
});
