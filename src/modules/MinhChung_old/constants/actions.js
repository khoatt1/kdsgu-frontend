import { createAction } from "redux-actions";
import { MODULE_NAME } from "../../MinhChung/constants/models";

export const fetchMemberPending = createAction(`@${MODULE_NAME}/FETCH_MEMBER_PENDING`);
export const setEmpty = createAction(`@${MODULE_NAME}/SET_EMPTY`);
export const setFilter = createAction(`@${MODULE_NAME}/SET_FILTER`);
export const resetFilter = createAction(`@${MODULE_NAME}/RESET_FILTER`);
export const fetchMemberSuccess  = createAction(`@${MODULE_NAME}/FETCH_MEMBER_SUCCESS`);
export const fetchMemberFailure  = createAction(`@${MODULE_NAME}/FETCH_MEMBER_FAILURE`);
export const testAPI  = createAction(`@${MODULE_NAME}/testAPI`);
export const minhChungAPI  = createAction(`@${MODULE_NAME}/minhChungAPI`);
export const clearData  = createAction(`@${MODULE_NAME}/clearData`);
export const minhChungAPIById  = createAction(`@${MODULE_NAME}/minhChungAPIById`);