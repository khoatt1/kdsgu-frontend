import { clearAll } from "../../../common/redux/actions/uiActions";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

const initialState = {
  isLoading: false,
  isEmpty: false,
  error: null,
  data: [],
  filter: {
    role: [],
    offset: 1,
    limit: 5,
    orderBy: "Name"
  },
  test_api: "tantm: test_api_null",
  minhChungList:[],
  minhchungDetail:null
};
//dua vao action dispatch vao de update state tuong ung
const handler = {
  [clearAll]: (state, action) => ({ ...initialState }),
  // FETCH MEMBER
  [actions.fetchMemberPending]: (state, action) => ({
    ...state,
    isLoading: true,
  }),
  [actions.fetchMemberSuccess]: (state, action) => ({
    ...state,
    isLoading: false,
    data: action.payload,
  }),
  [actions.fetchMemberFailure]: (state, action) => ({
    ...state,
    isLoading: false,
    error: action.payload,
  }),

  [actions.setFilter]: (state, action) => ({
    ...state,
    filter: action.payload,
  }),
  [actions.resetFilter]: (state, action) => ({
    ...state,
    filter: initialState.filter,
  }),
  // tantm
  [actions.testAPI]: (state, action) => ({
    ...state,
    test_api: action.payload
  }),
  [actions.minhChungAPI]: (state, action) => ({
    ...state,
    minhChungList: action.payload
  }),
  [actions.minhChungAPIById]: (state, action) => ({
    ...state,
    minhchungDetail: action.payload
  }),
  [actions.clearData]: (state, action) => ({ ...initialState }),
};

export default handleActions(handler, initialState);
