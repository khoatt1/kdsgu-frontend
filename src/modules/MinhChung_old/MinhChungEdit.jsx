import React, { useState, useMemo, useEffect } from "react";
import TextField from '@material-ui/core/TextField'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import { makeStyles } from '@material-ui/core/styles';
import handler from "./constants/handler";
import { useSelector, useDispatch,connect } from "react-redux";//connect để ket noi voi store
import { MODULE_NAME } from "./constants/models";
import { setSessionLoading } from "../../common/redux/actions/session";
import { useHistory,useParams } from "react-router-dom";

const useStyles = makeStyles({
  backgroundForm: {
    padding: "5px"   
  }
});

export default function MinhChungEdit(props) {  
      //get param from url
      let { idminhchung } = useParams();
      const [chiTietMinhChung, setChiTietMinhChung] = useState({});     
      const classes = useStyles();
      const dispatch = useDispatch();
      const history = useHistory();
      const minhchungDetail =  useSelector(state => state.minhchung.minhchungDetail);
      const {callMinhChungAPIById} = useMemo(() => handler(dispatch, props), [
          dispatch,
          props,
      ]);
     
      console.log('props: ',minhchungDetail?.id);   
      
    
      useEffect(async () => {
        callMinhChungAPIById(idminhchung);        
      }, [])
  return (
    <div className="container-fluid">
        <p> You were redirected from {minhchungDetail?.tenminhchung}</p>
        <div className="row p-2">     
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">ID minh chứng</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <input type="text" name="" id="input" className="form-control"  value={minhchungDetail?.tenminhchung}></input>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <span className="label">Tên minh chứng</span>
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <input type="text" name="" id="input" className="form-control" />
            </div>
        </div>       
       <div className="row">              
          <div className="col-xs- col-sm- col-md- col-lg- text-center p-2">             
            <button type="button" className="btn btn-primary" >Lưu</button>
            <span> </span>            
                <button type="button" className="btn btn-warning" 
                onClick={() => {history.push('/minhchung')}}>Quay lại</button>
          </div>          
       </div>
    </div>
  )
}