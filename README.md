# Technologies that you need to know to fully use this source
* Reactjs (React hook)
* Redux (to handle state globally)
* Material UI Kit (for making UI faster with pre-built components)
* React Router Dom (for changing page)
* Axios (for fetching API)

# Things must be pre-install on your machine
* npm (>= v6) & nodejs (>= v10)
* If you are using VSCode use should install **prettier** and **eslint** extension to have a lint check and code formatter

# How to run this source
1. npm i
2. npm run start

# Some useful stuff that good to know
* If you want to connect your react app to API, so the root API config is placed at /src/config/index.js
* Want to add a new page? 
    * Create a new page at src/pages
    * Go src/common/routes.jsx to add a new Route and map that route with the new page
* Each main part of project is store as a module in src/modules (like Student management will has it own module Student that including create, update, list, delete Student) make sure you follow this good structure